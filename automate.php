<?php

class table
{
  const MIN = 1;
  const MAX = 100;
  const TAB_LEN = 20;

  /**
   * methode qui fablique un tableau qui contient 20 nombre aléatoire
   * @return [array] $tab_random
   */
  static function makeTableRandom(){
    $tab = range(self::MIN, self::MAX);
    return $tab_random = array_rand($tab, self::TAB_LEN);
  }
}

class calculation
 {
   /**
    * [sumOfTwoNumber description]
    * methode qui calcule si la somme de deux nombres dans le tableauest
    * est égale au nombre saisi
    * @param  [int] $num
    * @return [string]
    */
   static function sumOfTwoNumber($num)
   {
     $i = 0;
     $tab_random = table::makeTableRandom();
     while($i < table::TAB_LEN)
     {
       $j = $i + 1;
       while ($j < table::TAB_LEN)
       {
         // test si la somme de deux nombres est égale au nombre saisi
         if ($tab_random[$i] + $tab_random[$j] == $num){
           // echo $tab_random[$i] . "\n";
           // echo $tab_random[$j] . "\n";
           return 'true';
         }
         else {
           $j++;
         }
       }
       $i++;
     }
     return 'false';
   }
 }

class Terminal
{
  /**
   * [interfaceTerminal description]
   * une interface en ligne de commande
   * elle demande à l'utilisateur de saisir un nombre
   * et affiche true ou false
   */
  static function interfaceTerminal(){
    do {
      echo 'Saisissez un nubmre entier entre 1 et 100' . "\n";
      // récupère un nombre saisi par l'utilisateur
      $num = trim(fgets(STDIN));
    } while ($num < table::MIN || table::MAX < $num);
    $answer = calculation::sumOfTwoNumber($num);
    echo $answer;
  }
}

Terminal::interfaceTerminal();
