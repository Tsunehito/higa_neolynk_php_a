<?php

class Produits
{
  private $name;
  private $reference;

  public function __construct($name, $reference)
  {
    $this->name = $name;
    $this->reference = $reference;
  }

  public function display()
  {
    return $this->reference . ' : ' . $this->name . '<br>';
  }
}

class CatalogueFactory
{
  public static function create($name, $reference)
  {
    return new Produits($name, $reference);
  }
}

$dentifrice = CatalogueFactory::create('Calgate', '00001');
$shampooing = CatalogueFactory::create('L\'Oréal', '00015');
$farines = CatalogueFactory::create('Farine de blé', '00201');

echo $dentifrice->display();
echo $shampooing->display();
echo $farines->display();
